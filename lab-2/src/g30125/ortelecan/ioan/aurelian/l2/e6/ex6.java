package g30125.ortelecan.ioan.aurelian.l2.e6;

import java.util.Scanner;

public class ex6 {
	public static void main(String[] args){
	       Scanner in = new Scanner(System.in);
	       System.out.println("Input a for Non-Recursive method OR b for Recursive Method:");
	       String a = in.nextLine();
	       System.out.println("Input the number:");
	       int n = in.nextInt();
	       if(a.equals("a"))
	       {
	    	   int p=1;  
	    	   for(int i=1; i<=n; i++)
	    	   {
	    		   p=p*i;
	    	   }
	    	   System.out.println(n+"!="+p);
	       }
	       else if(a.equals("b"))
	       {
	    	   
	    	   System.out.println(n+"!="+factorial(n));
	    		  
    		}
	   		in.close();
	}
	private static long factorial(int i) {

        if(i<0)  System.out.println("N must be > 0");
        if(i==0) return 1;
        else return i*factorial(i-1);
    }
}