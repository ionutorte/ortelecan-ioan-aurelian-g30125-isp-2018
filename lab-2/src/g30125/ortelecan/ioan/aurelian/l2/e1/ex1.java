package g30125.ortelecan.ioan.aurelian.l2.e1;

import java.util.Scanner;

public class ex1 {
   public static void main(String[] args){
       Scanner in = new Scanner(System.in);
       int x = in.nextInt();
       int y = in.nextInt();
 
       if(x==y)
    	   System.out.println("X=Y");
	   else
	   {
	       if(x>y)
	    	   System.out.println("X > Y");
	       else
			   System.out.println("Y<X");
	   }
       in.close();
   } 
}  
