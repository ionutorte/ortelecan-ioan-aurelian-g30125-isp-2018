package g30125.ortelecan.ioan.aurelian.l2.e2;

import java.util.Scanner;

public class ex2 {
	public static void main(String[] args){
	       Scanner in = new Scanner(System.in);
	       System.out.println("Input a for If method OR b for Switch Method");
	       String a = in.nextLine();
	       System.out.println("Input the number");
	       int x = in.nextInt();
	       
	       if(a.equals("a"))
	       {
		       if(x==1)
		    	   System.out.println("ONE");
		       if(x==2)
		    	   System.out.println("TWO");
		       if(x==3)
		    	   System.out.println("THREE");
		       if(x==4)
		    	   System.out.println("FOUR");
		       if(x==5)
		    	   System.out.println("FIVE");
		       if(x==6)
		    	   System.out.println("SIX");
		       if(x==7)
		    	   System.out.println("SEVEN");
		       if(x==8)
		    	   System.out.println("EIGHT");
		       if(x==9)
		    	   System.out.println("NINE");
		       if(x<1 || x>9)
		    		   System.out.println("OTHER");
	       }
	       if(a.equals("b"))
	       {
	    	   switch (x) {
	    	    case 1:  System.out.println("ONE");
	                     break;
	            case 2:  System.out.println("TWO");
	                     break;
	            case 3:  System.out.println("THREE");
	                     break;
	            case 4:  System.out.println("FOUR");
	                     break;
	            case 5:  System.out.println("FIVE");
	                     break;
	            case 6:  System.out.println("SIX");
	                     break;
	            case 7:  System.out.println("SEVEN");
	                     break;
	            case 8:  System.out.println("EIGHT");
	                     break;
	            case 9:  System.out.println("NINE");
	                     break;
	            case 10: System.out.println("TEN");
	                     break; 
	            default: System.out.println("OTHER");
	                     break;
	    	   }
	       }
	       in.close();
	    	   
	}
}
