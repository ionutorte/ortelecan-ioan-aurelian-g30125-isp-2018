package g30125.ortelecan.ioan.aurelian.l2.e4;

import java.util.Scanner;

public class ex4 {
	public static void main(String[] args){
	       Scanner in = new Scanner(System.in);
	       System.out.println("Dati numarul de elemente n:");
	       int n = in.nextInt();
	       int[] v = new int[n+1];
	       int max=-999;
	       for (int i = 1; i <= n; i++) 
	       {
	    	   System.out.println("Dati elementul "+i);
	    	   v[i]=in.nextInt();
	       }
	       
	       for (int i = 1; i <= n; i++) 
	       {
	    	   if(v[i]>max) max=v[i];
	       }
	       System.out.println("Maximul este "+max);
	       in.close();
	}
}
