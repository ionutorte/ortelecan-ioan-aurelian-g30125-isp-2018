package g30125.ortelecan.ioan.aurelian.l2.e7;

import java.util.*;

public class ex7 {
	public static void main(String[] args){
	       Scanner in = new Scanner(System.in);
	       Random r = new Random();
	       int x=r.nextInt(10);
	       int retries=0;
	       do
	       {
	    	   System.out.println("Welcome. Guess the number (0-10)");
	    	   int n = in.nextInt();
    		   if(n>x)
    		   {
	    		   System.out.println("Wrong answer, your number it too high");
	    		   retries++;
	    		   if(retries == 3) System.out.println("You lost.");
    		   }
    		   if(n==x)
    	       {
    	    	   System.out.println("Good Job. You guess the number :)");
    	    	   break;   
    	       }
    		   if(n<x)
    		   {
	    		   System.out.println("Wrong answer, your number is too low");
	    		   retries++;
	    		   if(retries == 3) System.out.println("You lost.");
    		   }
    	   }
	       while (retries<3);
	      in.close();
	}
}
