package g30125.ortelecan.ioan.aurelian.l3.ProblemaTest;

public class Solution {
	public static int solution(int[] A) 
	{
	    int result = 0;
	    for (int x : A) result ^= x; // XOR - x XOR x = 0 & asociativitate
	    return result;
	}

    public static void main(String[] args) {
  
        int[] A = new int[7];
        A[0] = 9;  A[1] = 3;  A[2] = 9;
        A[3] = 3;  A[4] = 9;  A[5] = 7;
        A[6] = 9;
        System.out.println(solution(A));
    }

}



