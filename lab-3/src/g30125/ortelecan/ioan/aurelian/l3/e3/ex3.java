package g30125.ortelecan.ioan.aurelian.l3.e3;

import becker.robots.*;

public class ex3 {
	public static void main(String[] args)
	{
		City ny = new City();
		Robot mark = new Robot(ny, 1, 1, Direction.NORTH);
		
		mark.move();
		mark.move();
		mark.move();
		mark.move();
		mark.move();
		
		mark.turnLeft();
		mark.turnLeft();
		
		mark.move();
		mark.move();
		mark.move();
		mark.move();
		mark.move();
	}

}
