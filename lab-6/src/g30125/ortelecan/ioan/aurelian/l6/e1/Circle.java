package g30125.ortelecan.ioan.aurelian.l6.e1;

import java.awt.*;

public class Circle extends Shape{

    private int radius,cx,cy;
    private String id;
    private boolean fill;

    public Circle(Color color, int radius, int cx, int cy,String id,boolean fill) {
        super(color);
        this.radius = radius;
        this.cx=cx;
        this.cy=cy;
        setFill(fill);
        setId(id);
    }

    public int getRadius() {
        return radius;
    }


    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString()+ "ID: " +getId());
        g.setColor(getColor());
        if (getfill()==true) {
            g.fillOval(cx, cy, radius, radius);
        }
        g.drawOval(cx,cy,radius,radius);
    }
}
