package g30125.ortelecan.ioan.aurelian.l6.e1;


import java.awt.*;

public abstract class Shape {

    private Color color;
    private String id;
    private boolean fill;
    public void setId(String id) {this.id = id; }
    public String getId() { return id;}

    public void setFill(boolean fill) {this.fill = fill; }
    public boolean getFill() { return fill;}

    public Shape(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    public void setfill(boolean fill) {
        this.fill = fill;
    }
    public boolean getfill() {
        return fill;
    }

    public abstract void draw(Graphics g);
}
