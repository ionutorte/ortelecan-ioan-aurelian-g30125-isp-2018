package g30125.ortelecan.ioan.aurelian.l6.e1;

import java.awt.*;
import java.util.Scanner;

/**
 * @author mihai.hulea
 */
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x,y;
        DrawingBoard b1 = new DrawingBoard();

        System.out.println("Dati coordonata x a cercului nr.1: ");
        x = in.nextInt();
        System.out.println("Dati coordonata y a cercului nr.1: ");
        y = in.nextInt();
        Shape s1 = new Circle(Color.RED, 90,x,y,"c2",true);
        b1.addShape(s1);

        System.out.println("Dati coordonata x a cercului nr.2: ");
        x = in.nextInt();
        System.out.println("Dati coordonata y a cercului nr.2 ");
        y = in.nextInt();
        Shape s2 = new Circle(Color.GREEN, 100,x,y,"c1",false);
        b1.addShape(s2);

        System.out.println("Dati coordonata x a dreptunghiului: ");
        x = in.nextInt();
        System.out.println("Dati coordonata y a dreptunghiului ");
        y = in.nextInt();
        Shape s3 = new Rectangle(Color.ORANGE,100,140,x,y,"abc",true);
        b1.addShape(s3);

        b1.deleteById("c2");

    }
}
