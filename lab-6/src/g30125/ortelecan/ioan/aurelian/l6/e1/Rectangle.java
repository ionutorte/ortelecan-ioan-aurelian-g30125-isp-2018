package g30125.ortelecan.ioan.aurelian.l6.e1;


import java.awt.*;

public class Rectangle extends Shape{

    private int width,height,cx,cy;
    private String id;
    private boolean fill;

    public Rectangle(Color color, int width, int height,int cx, int cy,String id,boolean fill) {
        super(color);
        this.width = width;
        this.height = height;
        this.cx=cx;
        this.cy=cy;
        setFill(fill);
        setId(id);
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(getColor());
        if (getfill()==true) {
            g.fillRect(cx, cy, width, height);
        }
        else
        g.drawRect(cx,cy,width,height);
        System.out.println("Drawing a rectangle "+width+" "+height+" "+getColor().toString()+"ID: " +getId());
    }
}
