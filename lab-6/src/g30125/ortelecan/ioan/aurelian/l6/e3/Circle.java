package g30125.ortelecan.ioan.aurelian.l6.e3;
import java.awt.*;
public class Circle implements Shape{
     int x=60;
     int y=80;
     String id="aa";
     boolean fill=true;
     Color color;
     int radius;

    public Circle(Color color, int radius) {
       this.color=color;
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public Color getColor() {
        return color;
    }
    public void setColor(Color color) {
        this.color = color;
    }
    public int getx() {
    	return x;
    }
    public int gety() {
    	return y;
    }
    public void setx(int x) {
    	this.x= x;
    }
    public void sety(int y) {
    	this.y= y;
    }
    public void setId(String id) {
    	this.id = id;
    }
    public String getId() {
    	return id;
    }
    public void setfill(boolean fill) {
    this.fill = fill;
    }
    public boolean getfill() {
    	return fill;
    }
    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        if(getfill()==true)
        	g.fillOval(getx(), gety(), radius, radius);
        else {
            g.drawOval(getx(), gety(), getRadius(), getRadius());
            g.drawString(getId(), getx(), gety());
        }
    }
}