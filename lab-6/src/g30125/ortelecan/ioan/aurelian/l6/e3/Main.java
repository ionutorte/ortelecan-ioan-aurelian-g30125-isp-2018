package g30125.ortelecan.ioan.aurelian.l6.e3;

import java.awt.*;
/**
 * @author mihai.hulea
 */
public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();

        Shape s1 = new Circle(Color.RED, 90);
        b1.addShape(s1);

        Shape s2 = new Circle(Color.GREEN, 100);
        b1.addShape(s2);

        Shape s3 = new Rectangle(Color.ORANGE,100,140);
        b1.addShape(s3);

    }
}
