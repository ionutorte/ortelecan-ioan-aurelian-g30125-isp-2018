
package g30125.ortelecan.ioan.aurelian.l6.e3;
import java.awt.Color;
import java.awt.Graphics;

public interface Shape {
    public Color getColor();
    public void setColor(Color color);
    public void setfill(boolean fill);
    public boolean getfill();
    public void setId(String id);
    public String getId();
    public void draw(Graphics g);
}
