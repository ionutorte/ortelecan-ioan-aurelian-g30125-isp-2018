package g30125.ortelecan.ioan.aurelian.l6.e5;
import java.awt.Color;

import g30125.ortelecan.ioan.aurelian.l6.e1.DrawingBoard;
import g30125.ortelecan.ioan.aurelian.l6.e1.Rectangle;
import g30125.ortelecan.ioan.aurelian.l6.e1.Shape;
public class Pyramid extends Rectangle{
	
	public Pyramid(Color color, int width, int height,int cx, int cy,String id,boolean fill) {
		super(color,width,height,cx,cy,id,fill);
		// TODO Auto-generated constructor stub
	}
	private static final int nr = 3;
	private static final int width = 32;
	private static final int length = 15;
	
	public static void createPyramid() {
		  DrawingBoard b1 = new DrawingBoard();
		int i,j,c=0,k=nr;
		
		for(i=0;i<k;i++) {			
			for(j=i;j>0;j--) {

			Shape brick = new Rectangle(Color.RED,140,100,100+length*(j+1)+(k-i)*length/2,300-width*(k-i+1),"abcd",false);
				brick.setId(" ");
				b1.addShape(brick);
				c++;
				
			}
		} 
	}
	public static void main(String[] args) {
		createPyramid();
	}
}