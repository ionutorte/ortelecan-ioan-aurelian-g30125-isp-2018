package g30125.ortelecan.ioan.aurelian.l7.e3;

public class BankAccount
{
    private String owner;
    private double balance;
    public BankAccount(){

    }
    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance=balance;
    }

    public double getBalance() {
        return this.balance;
    }

    public String getOwner() {
        return this.owner;
    }

    public void withdraw(double amount) {
        this.balance=this.balance-amount;

    }

    public void deposit(double amount) {
        this.balance=this.balance+amount;

    }
    public String toString() {
        return "Nume: "+owner+" Balanta: "+balance;
    }



}