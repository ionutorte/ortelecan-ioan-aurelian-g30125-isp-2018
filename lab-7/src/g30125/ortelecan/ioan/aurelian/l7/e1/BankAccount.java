package g30125.ortelecan.ioan.aurelian.l7.e1;

public class BankAccount
{
    private String Owner;
    private double balance;

    public void withdraw(double amount) {
        this.balance=this.balance-amount;

    }

    public void deposit(double amount) {
        this.balance=this.balance+amount;

    }

    public static void main(String args[]) {
        BankAccount cont = new BankAccount();
        BankAccountObjects x = new BankAccountObjects("Marcel",120000);
        BankAccountObjects y = new BankAccountObjects("Marcel",120000);
        if(x.equals(y)) System.out.println("Da"); else System.out.println("Nu");

    }

}

class BankAccountObjects {
    private String owner;
    private int balance;
    public BankAccountObjects(String owner, int balance) {
        this.owner = owner;
        this.balance=balance;
    }
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof BankAccountObjects))
            return false;
        BankAccountObjects x = (BankAccountObjects) obj;
        return owner.equals(x.owner);
    }


    public int hashCode() {
        return (int) (owner.length() * 1000);
    }
}

