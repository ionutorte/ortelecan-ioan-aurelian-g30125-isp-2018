package g30125.ortelecan.ioan.aurelian.l7.e4;
import java.util.*;
import java.io.*;

public class Dictionary {

    HashMap dictionary = new HashMap();

    public void addWord(Word c, Definition definitie) {

        if(dictionary.containsKey(c))
            System.out.println("Modific cuvant existent!");
        else
            System.out.println("Adauga cuvant nou.");
        dictionary.put(c, definitie);
    }

    public void getAllWords()
    {
            for (Object key : dictionary.keySet())
            {
                System.out.println(key);
            }
    }

    public void getAllDefinitions()
    {
        for (Object key : dictionary.keySet())
        {
            Object value = dictionary.get(key);
            System.out.println("DEFINITION FOR "+key+": " + value);
        }
    }


    public static void main(String args[]) throws Exception {
        Dictionary dict = new Dictionary();
        char raspuns;
        String linie, explic;
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("Meniu");
            System.out.println("a - Adauga cuvant");
            System.out.println("d - Listeaza definitii");
            System.out.println("l - Listeaza cuvinte");
            System.out.println("e - Iesi");

            linie = fluxIn.readLine();
            raspuns = linie.charAt(0);

            switch(raspuns) {
                case 'a': case 'A':
                    System.out.println("Introduceti cuvantul:");
                    linie = fluxIn.readLine();
                    if( linie.length()>1) {
                        System.out.println("Introduceti definitia:");
                        explic = fluxIn.readLine();
                        dict.addWord(new Word(linie), new Definition(explic));
                    }
                    break;
                case 'l': case 'L':
                    System.out.println("Afisare cuvinte:");
                    dict.getAllWords();
                    break;
                case 'd': case 'D':
                    System.out.println("Afisare definitii:");
                    dict.getAllDefinitions();
                    break;

            }
        } while(raspuns!='e' && raspuns!='E');
        System.out.println("Program terminat.");
    }
}
class Definition{
    String c;
    public Definition(String c) {
        this.c = c;
    }

    public String toString() {
        return c;
    }

}

class Word{
    String c;
    public Word(String c) {
        this.c = c;
    }

    public String toString() {
        return c;
    }
}