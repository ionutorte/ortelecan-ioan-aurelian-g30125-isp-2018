package g30125.ortelecan.ioan.aurelian.l4.e8;

public class Rectangle extends Shape 
{
	
	private double width,length;
	
	public Rectangle() {
		width=length=1.0;
	}
	public Rectangle(double width,double length) {
		this.width=width;
		this.length=length;
	}
	public Rectangle(double width,double length,String color,boolean filled) {
		super(color,filled);
		this.width=width;
		this.length=length;
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width=width;
	}
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length=length;
	}
	public double getArea() {
		return width*length;
	}
	public double getPerimeter() {
		return 2*(width*length);
	}
	public String toString() {
		super.toString();
		return "A Rectangle with width="+getWidth()+" and length="+getLength()+",which is a subclas of "+super.toString()+" method from the superclass";
	}
}
