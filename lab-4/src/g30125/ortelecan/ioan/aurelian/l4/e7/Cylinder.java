package g30125.ortelecan.ioan.aurelian.l4.e7;
import g30125.ortelecan.ioan.aurelian.l4.e3.Circle;

public class Cylinder extends Circle 
{
	private double height;
	
	public Cylinder() 
	{
		height=1.0;
	}
	public Cylinder(double radius) 
	{
		super(radius);
		this.height=10;
	}
	public Cylinder(double radius, double height) 
	{
		super(radius);
		this.height=height;
	}
	public double getHeight() 
	{
		return height;
	}
	public double getArea() 
	{
		super.getArea();
		return Math.PI*Math.pow(super.getRadius(),2);
	}
	public double getVolume() 
	{
		return getHeight()*getArea();
	}
}
