package g30125.ortelecan.ioan.aurelian.l4.e6;

import g30125.ortelecan.ioan.aurelian.l4.e4.Author;

public class Book 
{
	private String name;
	private Author[] author;
	private double price;
	private int qtyInStock=0;
	public Book (String name, Author[] author, double price) {
		this.name=name;
		this.author=author;
		this.price=price;
	}
	public Book (String name, Author author,int pos, double price,int qtyInStock) {
		this.name=name;
		this.author[pos]=author;
		this.price=price;
		this.qtyInStock=qtyInStock;
	}
	public String getName() {
		return name;
	}

	public Author[] getAuthor() {
		return author;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getQtyInStock() {
		return qtyInStock;
	}
	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock = qtyInStock;
	}
	public String toString() {
		return "book name "+"by "+author.length+" authors";
	}
	public void printAuthors() {
		System.out.println(author[0].getName());
		System.out.println(author[1].getName());
		System.out.println(author[2].getName());
	}

}
