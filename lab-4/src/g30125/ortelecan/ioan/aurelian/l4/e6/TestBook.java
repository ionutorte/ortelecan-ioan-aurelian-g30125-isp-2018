package g30125.ortelecan.ioan.aurelian.l4.e6;
import g30125.ortelecan.ioan.aurelian.l4.e4.Author;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestBook {

	@Test
	public void testToString() {
		Author[] author = new Author[2];
		author[0]=new Author("marc","a@yahoo.com",'F');
		author[1]=new Author("author1","b@yahoo.com",'M');
		Book b = new Book("b",author,78);
		assertEquals("book name by 2 authors",b.toString());
		
	}
	
	@Test
	public void testPrintAuthors() {
		Author[] author = new Author[2];
		author[0]=new Author("author","a@yahoo.com",'F');
		author[1]=new Author("author1","b@yahoo.com",'M');
		
		assertEquals("author",author[0].getName());
		assertEquals("author1",author[1].getName());
		
		
	}

}