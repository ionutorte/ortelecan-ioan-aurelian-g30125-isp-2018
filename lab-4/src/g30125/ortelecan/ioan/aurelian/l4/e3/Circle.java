package g30125.ortelecan.ioan.aurelian.l4.e3;

public class Circle {
	private double radius;
	private String color;
	
	public Circle()
	{
		radius = 1.0;
		color="red";
	}
	
	public Circle(double raza)
	{
		radius=raza;
		color="red";
	}
	
	public double getRadius()
    {
        return radius;
    }
	public double getArea()
    {
        return 3.14*Math.pow(radius, 2);
    }
	public static void main (String[] args)
	{
		Circle crc = new Circle(4.2);
		System.out.println(crc.getRadius()+","+crc.color);
		System.out.println(crc.getArea());
		
	}

}
