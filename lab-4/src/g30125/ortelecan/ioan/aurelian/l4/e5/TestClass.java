package g30125.ortelecan.ioan.aurelian.l4.e5;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import g30125.ortelecan.ioan.aurelian.l4.e4.Author;
import g30125.ortelecan.ioan.aurelian.l4.e5.Book;

public class TestClass {
	
	 @Test
	    public void TestBook(){
	        Author a = new Author("Marcel","marcel@gmail.com",'M');
	        Book b = new Book("Capra cu trei iezi",a,5.5);
	        assertEquals(" '"+b.getName()+"' by "+a.getName()+" ("+a.getGender()+") at email: "+a.getEmail(),b.toString());
	    }
}
