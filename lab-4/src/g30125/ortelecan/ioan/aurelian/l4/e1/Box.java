package g30125.ortelecan.ioan.aurelian.l4.e1;

/**
 * @author mihai.hulea
 */
public class Box {
    private int id;

    public Box(Conveyor target, int pos, int id){
        this.id = id;
        target.addPackage(this,pos);
    }

    public int getId() {
        return id;
    }

    public String toString(){
        return ""+id;
    }
}
