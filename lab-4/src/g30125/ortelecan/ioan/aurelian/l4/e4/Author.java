package g30125.ortelecan.ioan.aurelian.l4.e4;

public class Author {
	private String name,email;
	private char gender;
	public Author(String nume, String email1, char gen)
	{
		name=nume;
		email=email1;
		gender = gen;
	}
	public String getName()
    {
        return name;
    }
    public String getEmail()
    {
        return email;
    }
    public char getGender()
    {
        return gender;
    }
    public void setEmail(String email1)
    {
    	this.email=email1;
    }
    void ShowAuthor() {
        System.out.println("Author-"+name+"("+gender+") at "+email);
    }
    
    public static void main(String[] args)
    {
    	Author a1 = new Author("Marcel","marcel@gmail.com",'M');
    	a1.ShowAuthor();
    	System.out.println("Gender: "+a1.getGender());
    	System.out.println("Name: "+a1.getName());
    	System.out.println("Email: "+a1.getEmail());
    	a1.setEmail("marcel2@gmail.com");
    	System.out.println("Email: "+a1.getEmail());
    	
    	
    }
}
