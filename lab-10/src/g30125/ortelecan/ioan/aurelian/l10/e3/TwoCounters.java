package g30125.ortelecan.ioan.aurelian.l10.e3;

public class TwoCounters extends Thread {

    int k1=0,k2=100;
    String n;
    Thread t;
    TwoCounters(String n, Thread t){this.n = n;this.t=t;}

    public void run()
    {
        System.out.println("Firul "+n+" a intrat in metoda run()");
        try
        {
            if (t!=null) { k1=100;k2=200;t.join();}
            System.out.println("Firul "+n+" executa operatie.");
            for(int i=k1;i<=k2;i++){
                System.out.println(getName() + " i = "+i);}
            Thread.sleep(300);
            System.out.println("Firul "+n+" a terminat operatia.");
        }
        catch(Exception e){e.printStackTrace();}

    }


    public static void main(String[] args) {
        TwoCounters c1 = new TwoCounters("counter1",null);
        TwoCounters c2 = new TwoCounters("counter2",c1);

        c1.run();
        c2.run();
    }
}
