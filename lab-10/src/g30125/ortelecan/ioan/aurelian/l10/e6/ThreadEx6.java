package g30125.ortelecan.ioan.aurelian.l10.e6;

public class ThreadEx6 extends Thread{

    public static void main(String[] args) {
        Punct p = new Punct();
        FirSet fs1 = new FirSet(p);
        fs1.start();
    }
}
class FirSet extends ThreadEx6 {
    Punct p;
    public static int s=0,m=0,h=0;
    public FirSet(Punct p){
        this.p = p;
    }
    private void pauseThread ()
    {
        synchronized (p)
        {
            if (!Chronometer.ok)
            {try {
                s=s-1;
                p.wait();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            }// Note that this can cause an InterruptedException
        }
    }
    public void run(){
        synchronized(p){
            p.notify();
            p.setSMH(s,m,h);
        }
        while(s<60){
            s++;
            if((m==60)&&(s==60))
            {
                h++;m=0;s=0;
            }
            else
            if(s==60)
            {
                m++;s=0;
            }

            try {
                sleep(1000);

            } catch (InterruptedException e) {

                e.printStackTrace();
            }
            pauseThread();
            System.out.println("["+h+","+m+","+s+"]");
            Chronometer.text.setText(FirSet.h+":"+FirSet.m+":"+FirSet.s);
        }
    }
}//.class

class Punct {
    int s,m,h;
    public void setSMH(int s,int m,int h){
        this.s = s;
        this.m = m;
        this.h=h;
    }
}