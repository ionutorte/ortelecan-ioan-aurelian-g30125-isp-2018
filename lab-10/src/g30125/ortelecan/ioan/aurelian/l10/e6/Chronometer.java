package g30125.ortelecan.ioan.aurelian.l10.e6;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.awt.Font;


@SuppressWarnings("serial")
public class Chronometer extends JFrame{

    JButton bReset;
    JButton bStartPause;
    public static JLabel text;

    Chronometer(){

        setTitle("Chronometer");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,200);
        setVisible(true);
    }

    public void init(){

        this.setLayout(null);
        int height = 20;

        text = new JLabel(FirSet.h+":"+FirSet.m+":"+FirSet.s);
        text.setBounds(45, 80, 140, 50);
        text.setFont(new Font("Serif", Font.PLAIN, 40));

        bReset = new JButton("Reset");
        bReset.setBounds(50,55,90, height);
        bReset.addActionListener(new TratareButonReset());

        bStartPause = new JButton("Start/Pause");
        bStartPause.setBounds(50,30,90, height);
        bStartPause.addActionListener(new TratareButonStartPause());

        add(bReset);
        add(bStartPause);
        add(text);
        Chronometer.text.setText(FirSet.h+":"+FirSet.m+":"+FirSet.s);

    }

    public static void main(String[] args) {
        new Chronometer();
    }
    class TratareButonReset implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            FirSet.s=0;
            FirSet.m=0;
            FirSet.h=0;
            Chronometer.text.setText(FirSet.h+":"+FirSet.m+":"+FirSet.s);
        }
    }
    public  static boolean ok=false;
    class TratareButonStartPause implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            if(!ok)
            {
                ok=true;
                ThreadEx6.main(null);
            }
            else
                ok=false;

        }
    }
}
