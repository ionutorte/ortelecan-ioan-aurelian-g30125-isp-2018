package g30125.ortelecan.ioan.aurelian.l10.e4;
import java.util.Random;

public class Robots extends Thread{
    int x,y;
    String name;
    static Robots r[]=new Robots[10];
    Robots(int x,int y,String name){
        this.x=x;
        this.y=y;
        this.name=name;
    }

    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }
    public String getRobot() {
        return name;
    }

    public void run() {
        while(true) {
            for(int i=0;i<10;i++)
            {	if(r[i]!=null) {Random rand = new Random();
                int limita=10;
                int ResultX = rand.nextInt(3);
                int ResultY = rand.nextInt(3);
                int k1=r[i].getX()+1-ResultX;
                int k2=r[i].getY()+1-ResultY;
                if((k1<0)||(k1>=limita))
                {
                    while((k1<0)||(k1>=limita))
                    {
                        int ResultX1 = rand.nextInt(3);
                        k1=r[i].getX()+1-ResultX1;
                    }
                }
                else
                    r[i].setX(k1);
                if((k2<0)||(k2>=limita))
                {
                    while((k2<0)||(k2>=limita))
                    {
                        int ResultY1 = rand.nextInt(3);
                        k2=r[i].getY()+1-ResultY1;
                    }
                }
                else
                    r[i].setY(k2);
            }
            }

            try {
                for(int i=0;i<9;i++) {
                    for(int j=i+1;j<10;j++) {
                        if(r[i]!=null&&r[j]!=null) {
                            System.out.println("X:Compare "+r[i].getX()+"  with  "+r[j].getX());
                            System.out.println("Y:Compare "+r[i].getY()+"  with  "+r[j].getY());
                        }
                        if(r[i]!=null&&r[j]!=null&&(r[i].getX()==r[j].getX())&&(r[i].getY()==r[j].getY())) {
                            System.out.println(r[i].getRobot()+" and "+r[j].getRobot()+" were distroyed!\n");
                            r[i]=null;r[j]=null;
                        }
                    }
                }
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public static void main(String[] args) {
        for(int i=0;i<10;i++)
        {	if(r[i]==null) {Random rand = new Random();
            int ResultX = rand.nextInt(10);
            int ResultY = rand.nextInt(10);
            r[i]=new Robots(ResultX,ResultY,"Robot "+i);
        }
        }
        for(int i=0;i<10;i++)
        {
            r[i].run();
        }
    }
}
