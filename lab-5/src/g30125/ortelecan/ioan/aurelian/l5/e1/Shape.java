package g30125.ortelecan.ioan.aurelian.l5.e1;

public abstract class Shape {
    protected String color;
    protected boolean filled;


    Shape(){

    }

    Shape(String color, boolean filled){
        this.color=color;
        this.filled=filled;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color=color;
    }

    public boolean isFilled() {
        return this.filled;
    }

    public void setFilled(boolean filled) {
        this.filled=filled;
    }

    abstract double getArea();

    abstract double getPerimeter();
    @Override
    public String toString() {
        if(this.filled==true)
            return "A shape with color "+this.color+" and is filled";
        else
            return "A shape with color "+this.color+" and is not filled";
    }

}