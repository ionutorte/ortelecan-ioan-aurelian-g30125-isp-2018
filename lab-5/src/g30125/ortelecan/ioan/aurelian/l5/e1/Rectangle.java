package g30125.ortelecan.ioan.aurelian.l5.e1;

public class Rectangle extends Shape {

    protected double width;
    protected double length;

    Rectangle(){
    }

    Rectangle(double width, double length){
        this.width=width;
        this.length=length;
    }

    Rectangle(String color, boolean filled, double width, double length){
        super(color,filled);
        this.width=width;
        this.length=length;
    }

    public double getWidth() {
        return this.width;
    }

    public void setWidth(double width) {
        this.width=width;
    }

    public double getLength() {
        return this.length;
    }

    public void setLength(double length) {
        this.length=length;
    }

    @Override
    public double getArea() {
        return this.width*this.length;
    }

    @Override
    public double getPerimeter() {
        return 2*(this.width+this.length);
    }

    @Override
    public String toString() {
        return "A rectangle with width "+getWidth()+" and length "+getLength()+" which is a subclass of "+super.toString();
    }

}
