package g30125.ortelecan.ioan.aurelian.l5.e1;

import static org.junit.Assert.*;

import org.junit.Test;

public class ShapeTest {

    @Test
    public void test() {
        Circle c=new Circle(1.0,"blue",true);
        assertEquals(c.toString(),"A circle with radius 1.0 which is a subclass of A shape with color blue and is filled");
    }

    @Test
    public void test1() {
        Rectangle c=new Rectangle("blue",true,1.0,1.0);
        assertEquals(c.toString(),"A rectangle with width 1.0 and length 1.0 which is a subclass of A shape with color blue and is filled");
    }

    @Test
    public void test2() {
        Square c=new Square(1.0,"blue",true);
        assertEquals(c.toString(),"A square with side 1.0 which is a subclass of A rectangle with width 1.0 and length 1.0 which is a subclass of A shape with color blue and is filled");
    }

}
