package g30125.ortelecan.ioan.aurelian.l5.e1;

public class Square extends Rectangle {
    protected double side;

    Square(){

    }

    Square(double side){
        this.side=side;
    }
    Square(double side, String color,boolean filled){
        super(color,filled,side,side);
        this.side=side;
    }
    public double getSide() {
        return this.side;
    }
    public void setSide(double side) {
        this.side=side;
    }
    public void setWidth(double side) {
        super.setWidth(side);
    }
    public void setLenght(double side) {
        super.setLength(side);
    }
    @Override
    public String toString() {
        return "A square with side "+getSide()+" which is a subclass of "+super.toString();
    }

}
