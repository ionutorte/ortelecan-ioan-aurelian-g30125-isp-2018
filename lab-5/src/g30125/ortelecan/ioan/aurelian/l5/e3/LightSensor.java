package g30125.ortelecan.ioan.aurelian.l5.e3;
import java.util.Random;

public class LightSensor extends Sensor{
    Random r=new Random();
    int val=r.nextInt(100);
    @Override
    int readValue() {
        return this.val;
    }

}
