package g30125.ortelecan.ioan.aurelian.l5.e2;

import static org.junit.Assert.*;

import org.junit.Test;

public class ProxyImageTest {
    @Test
    public void test() {
        ProxyImage c=new ProxyImage("imagine1",0);
        assertEquals(c.display(),"Displaying imagine1");
    }

    @Test
    public void test1() {
        ProxyImage c=new ProxyImage("imagine1",1);
        assertEquals(c.display(),"Display rotated imagine1");
    }


}