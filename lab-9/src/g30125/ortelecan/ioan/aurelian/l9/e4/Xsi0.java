package g30125.ortelecan.ioan.aurelian.l9.e4;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Xsi0 extends JFrame
{
    public int count = 0,win=0; //variabile globale
    public enum State
    {
        EMPTY,
        X,
        O
    }
    int[][] winIndices = {{0,1,2}, {3,4,5}, {6,7,8}, {0,3,6}, {1,4,7}, {2,5,8}, {0,4,8}, {2,4,6}}; //combinatiile posibile de castig

    JButton[] buttons = new JButton[10];
    State[] ButtonState = new State[10];

    Xsi0() {
        setTitle("X & 0 - ISP");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300, 400);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);

        buttons[0] = new MyButton(0);
        buttons[1] = new MyButton(1);
        buttons[2] = new MyButton(2);
        buttons[3] = new MyButton(3);
        buttons[4] = new MyButton(4);
        buttons[5] = new MyButton(5);
        buttons[6] = new MyButton(6);
        buttons[7] = new MyButton(7);
        buttons[8] = new MyButton(8);
        buttons[9] = new MyButton(9);
        buttons[9].setText("Good Luck!");

        buttons[0].setBounds(0, 0, 93, 103);
        buttons[1].setBounds(92, 0, 100, 103);
        buttons[2].setBounds(192, 0, 93, 103);
        buttons[3].setBounds(0, 103, 93, 103);
        buttons[4].setBounds(92, 103, 100, 103);
        buttons[5].setBounds(192, 103, 93, 103);
        buttons[6].setBounds(0, 206, 93, 103);
        buttons[7].setBounds(92, 206, 100, 103);
        buttons[8].setBounds(192, 206, 93, 103);
        buttons[9].setBounds(0, 306, 300, 63);

        for(int i=0;i<=9;i++)
        {
            ButtonState[i] = State.EMPTY;
            add(buttons[i]);
            buttons[i].addActionListener(new TratareButon());
        }
    }

    public static void main(String[] args) {
        new Xsi0();
    }

    public class MyButton extends JButton {
        private int index;

        public MyButton(int index) {
            super();
            this.index = index;
        }

        public int getIndex() {
            return index;
        }
    }

    class TratareButon implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            MyButton button2 = (MyButton) e.getSource();
            if(win==0) //Conditie folosita pentru a nu mai putea apasa butoanele dupa terminarea jocului
            {
                if(button2.getIndex() != 9) // verificam sa nu fie apasat butonul de index 9 (el va puta fi functional doar la terminarea jocului)
                {
                    count++;
                    if (count == 1 || count == 3 || count == 5 || count == 7 || count == 9) {
                        ButtonState[button2.getIndex()] = State.X;
                        button2.setText("X");
                        if (checkWin()) {
                            win = 1;
                            buttons[9].setText("X Wins! - Click here to reset");
                        }

                    } else {
                        ButtonState[button2.getIndex()] = State.O;
                        button2.setText("O");
                        if (checkWin()) {
                            win = 1;
                            buttons[9].setText("O Wins! - Click here to reset");
                        }
                    }
                    if(count==9 && !checkWin())
                    {
                        win = 1;
                        buttons[9].setText("Draw! - Click here to reset");
                    }
                }
            }
            else
            {
                if(button2.getIndex() == 9) ResetGame();
            }
        }
    }

    public boolean checkWin() {
        for(int[] indexArray : winIndices)
        {
            boolean same = (ButtonState[indexArray[0]] == ButtonState[indexArray[1]]) && (ButtonState[indexArray[1]] == ButtonState[indexArray[2]]);
            if(same && ButtonState[indexArray[0]] != State.EMPTY) {
                return true;
            }
        }
        return false;
    }

    public void ResetGame()
    {
        win=0;
        count=0;
        for(int i=0;i<=9;i++) {
            buttons[i].setText("");
            ButtonState[i] = State.EMPTY;
        }
        buttons[9].setText("Good Luck!");
    }
}