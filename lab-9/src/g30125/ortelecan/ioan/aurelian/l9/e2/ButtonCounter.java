package g30125.ortelecan.ioan.aurelian.l9.e2;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class ButtonCounter extends JFrame{

    JTextArea tArea;
    JButton bLoghin;

    ButtonCounter(){

        setTitle("Test Counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);
    }

    public void init(){

        this.setLayout(null);

        bLoghin = new JButton("Press This Button");
        bLoghin.setBounds(10,50,150, 20);

        bLoghin.addActionListener(new TratareButonLoghin());

        tArea = new JTextArea();
        tArea.setBounds(10,100,150,80);

        add(bLoghin);
        add(tArea);

    }

    public static void main(String[] args) {
        new ButtonCounter();
    }

    class TratareButonLoghin implements ActionListener{
        private int counter = 0;
        public void actionPerformed(ActionEvent e) {

            counter++;
            ButtonCounter.this.tArea.setText("");
            ButtonCounter.this.tArea.append("Count:"+counter+"\n"); }
        }
    }