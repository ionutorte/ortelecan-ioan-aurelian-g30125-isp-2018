package g30125.ortelecan.ioan.aurelian.l9.e3;
import java.io.File;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class FileDisplay extends JFrame {

    JLabel fname;
    JTextField tFname;
    JTextArea tArea;
    JButton bShow;

    FileDisplay() {

        setTitle("Test Display File");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200, 300);
        setVisible(true);
    }

    public void init()
    {
        this.setLayout(null);
        int width = 80;
        int height = 20;

        fname = new JLabel("Calea catre fisier: ");
        fname.setBounds(30, 50, 120, 40);

        tFname = new JTextField();
        tFname.setBounds(40, 80, width, height);

        bShow = new JButton("Show");
        bShow.setBounds(10, 150, width, height);

        bShow.addActionListener(new TratareButon());

        tArea = new JTextArea();
        tArea.setBounds(10, 180, 150, 80);

        add(fname);
        add(tFname);
        add(bShow);
        add(tArea);

    }

    public static void main(String[] args) {
        new FileDisplay();
    }

    class TratareButon implements ActionListener {

        public void actionPerformed(ActionEvent e)
        {
            String text = FileDisplay.this.tFname.getText();
            FileDisplay.this.tArea.setText("");

            try {
                Scanner input;
                File file = new File(text);
                if(file.exists() && !file.isDirectory()) {
                    input = new Scanner(file);

                    while (input.hasNextLine()) {
                        String line = input.nextLine();
                        FileDisplay.this.tArea.append(line+"\n");
                    }
                    input.close();
                }
                else  FileDisplay.this.tArea.append("Fisierul nu exista");


            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }
    }
}