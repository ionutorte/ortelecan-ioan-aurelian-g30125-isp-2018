package g30125.ortelecan.ioan.aurelian.l8.e2;

// Java Program to illustrate reading from text file
// as string in Java
import java.nio.file.*;

public class ReadText
{
    public static String readFileAsString(String fileName)throws Exception
    {
        String data = "";
        data = new String(Files.readAllBytes(Paths.get(fileName)));
        return data;
    }

    public static void main(String[] args) throws Exception
    {
        try{
            int counter=0;
            String data = readFileAsString("C:\\Users\\Orte\\Desktop\\test.txt");
            for(int i = 0; i< data.length();i++)
            {
                if( data.charAt(i) == 'e' )
                    counter++;
            }
            System.out.println("E character appear "+counter+" times");
        }
        finally {
        }
    }
}
