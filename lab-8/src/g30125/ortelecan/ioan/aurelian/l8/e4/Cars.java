package g30125.ortelecan.ioan.aurelian.l8.e4;
import java.io.*;

public class Cars {
    public static void main(String[] args) throws Exception{
        CarFactory f = new CarFactory();

        Car a = f.createCar("Passat",7000);
        Car b = f.createCar("BMW",1500);

        f.saveCar(a,"car1.dat");
        f.saveCar(b,"car2.dat");

        Car x = f.takeCar("car1.dat");
        Car y = f.takeCar("car2.dat");
    }

}//.class

class CarFactory{
    Car createCar(String model,int price){
        Car z = new Car(model,price);
        System.out.println(z+"-Car created");
        return z;
    }

    void saveCar(Car a, String storeRecipientName) throws IOException{
        ObjectOutputStream o =
                new ObjectOutputStream(
                        new FileOutputStream(storeRecipientName));

        o.writeObject(a);
        System.out.println(a+"-Car saved");
    }

    Car takeCar(String storeRecipientName) throws IOException, ClassNotFoundException{
        ObjectInputStream in =
                new ObjectInputStream(
                        new FileInputStream(storeRecipientName));
        Car x = (Car)in.readObject();
        System.out.println("Model:"+x.model+" - Price:"+x.price);
        return x;
    }


}

class Car implements Serializable{

    String model;
    int price;

    public Car(String model,int price) {
        this.model = model;
        this.price=price;
    }
    public String toString(){return "[CarModel="+model+" Price="+price+"]";}


}

